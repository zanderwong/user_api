create table user
(
    id           bigint unsigned auto_increment
        primary key,
    user_id      varchar(16)  default ''                not null,
    username     varchar(50)  default ''                not null,
    email        varchar(100) default ''                not null,
    display_name varchar(100) default ''                not null,
    is_blocked   tinyint(1) default 0 not null,
    ctime        timestamp    default CURRENT_TIMESTAMP not null,
    utime        timestamp    default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    constraint user_user_id_uindex
        unique (user_id),
    constraint user_username_uindex
        unique (username)
);

create
index user_is_blocked_index
    on user (is_blocked);

INSERT INTO test.user (id, user_id, username, email, display_name, is_blocked, ctime, utime)
VALUES (1, '1b4f0e9851971998', 'test1', 'test1@gmail.com', 'Alpha', 0, '2021-11-10 14:52:30', '2021-11-10 14:52:30');
INSERT INTO test.user (id, user_id, username, email, display_name, is_blocked, ctime, utime)
VALUES (2, '60303ae22b998861', 'test2', 'test2@gmail.com', 'Beta', 0, '2021-11-10 14:52:30', '2021-11-10 14:52:30');
INSERT INTO test.user (id, user_id, username, email, display_name, is_blocked, ctime, utime)
VALUES (3, 'fd61a03af4f77d87', 'test3', 'test3@gmail.com', 'Charlie', 0, '2021-11-10 14:52:30', '2021-11-10 14:52:30');
INSERT INTO test.user (id, user_id, username, email, display_name, is_blocked, ctime, utime)
VALUES (4, 'a4e624d686e03ed2', 'test4', 'test4@gmail.com', 'Delta', 0, '2021-11-10 14:52:30', '2021-11-10 14:52:30');
INSERT INTO test.user (id, user_id, username, email, display_name, is_blocked, ctime, utime)
VALUES (5, 'a140c0c1eda2def2', 'test5', 'test5@gmail.com', 'Echo', 0, '2021-11-10 14:52:30', '2021-11-10 14:52:30');