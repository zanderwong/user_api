package biz

import (
	"context"
	"time"

	"github.com/go-kratos/kratos/v2/log"
)

type User struct {
	ID          string
	Username    string
	Email       string
	DisplayName string
	IsBlocked   bool
	Ctime       time.Time
	Utime       time.Time
	Like        int64
}

type UserRepo interface {
	// db
	ListUser(ctx context.Context) ([]*User, error)
	GetUser(ctx context.Context, userID string) (*User, error)
	CreateUser(ctx context.Context, user *User) error
	UpdateUser(ctx context.Context, userID string, user *User) error
	DeleteUser(ctx context.Context, userID string) error

	// redis
	GetUserLike(ctx context.Context, userID string) (rv int64, err error)
	IncUserLike(ctx context.Context, userID string) error
}

type UserUsecase struct {
	repo UserRepo
}

func NewUserUsecase(repo UserRepo, logger log.Logger) *UserUsecase {
	return &UserUsecase{repo: repo}
}

func (uc *UserUsecase) List(ctx context.Context) (ps []*User, err error) {
	ps, err = uc.repo.ListUser(ctx)
	if err != nil {
		return
	}
	return
}

func (uc *UserUsecase) Get(ctx context.Context, userID string) (p *User, err error) {
	p, err = uc.repo.GetUser(ctx, userID)
	if err != nil {
		return
	}
	err = uc.repo.IncUserLike(ctx, userID)
	if err != nil {
		return
	}
	p.Like, err = uc.repo.GetUserLike(ctx, userID)
	if err != nil {
		return
	}
	return
}

func (uc *UserUsecase) Create(ctx context.Context, user *User) error {
	return uc.repo.CreateUser(ctx, user)
}

func (uc *UserUsecase) Update(ctx context.Context, userID string, user *User) error {
	return uc.repo.UpdateUser(ctx, userID, user)
}

func (uc *UserUsecase) Delete(ctx context.Context, userID string) error {
	return uc.repo.DeleteUser(ctx, userID)
}
