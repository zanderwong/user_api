package data

import (
	"context"

	"github.com/go-kratos/kratos/v2/log"
	"gitlab.com/zanderwong/user_api/internal/biz"
)

type userRepo struct {
	data *Data
	log  *log.Helper
}

// NewUserRepo .
func NewUserRepo(data *Data, logger log.Logger) biz.UserRepo {
	return &userRepo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (ar *userRepo) ListUser(ctx context.Context) ([]*biz.User, error) {
	ps, err := ar.data.db.User.Query().All(ctx)
	if err != nil {
		return nil, err
	}
	rv := make([]*biz.User, 0)
	for _, p := range ps {
		l, err := ar.GetUserLike(ctx, p.ID)
		if err != nil {
			l = 0
		}
		rv = append(rv, &biz.User{
			ID:          p.ID,
			Username:    p.Username,
			Email:       p.Email,
			DisplayName: p.DisplayName,
			IsBlocked:   p.IsBlocked,
			Ctime:       p.Ctime,
			Utime:       p.Utime,
			Like:        l,
		})

	}
	return rv, nil
}

func (ar *userRepo) GetUser(ctx context.Context, userID string) (*biz.User, error) {
	p, err := ar.data.db.User.Get(ctx, userID)
	if err != nil {
		return nil, err
	}
	return &biz.User{
		ID:          p.ID,
		Username:    p.Username,
		Email:       p.Email,
		DisplayName: p.DisplayName,
		IsBlocked:   p.IsBlocked,
		Ctime:       p.Ctime,
		Utime:       p.Utime,
	}, nil
}

func (ar *userRepo) CreateUser(ctx context.Context, user *biz.User) error {
	_, err := ar.data.db.User.
		Create().SetUsername(user.Username).SetEmail(user.Email).SetDisplayName(user.DisplayName).
		Save(ctx)
	return err
}

func (ar *userRepo) UpdateUser(ctx context.Context, userID string, user *biz.User) error {
	p, err := ar.data.db.User.Get(ctx, userID)
	if err != nil {
		return err
	}
	_, err = p.Update().
		SetUsername(user.Username).SetEmail(user.Email).SetDisplayName(user.DisplayName).SetIsBlocked(user.IsBlocked).
		Save(ctx)
	return err
}

func (ar *userRepo) DeleteUser(ctx context.Context, userID string) error {
	return ar.data.db.User.DeleteOneID(userID).Exec(ctx)
}
