package data

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
)

func likeKey(userID string) string {
	return fmt.Sprintf("like:%s", userID)
}

func (ar *userRepo) GetUserLike(ctx context.Context, userID string) (rv int64, err error) {
	get := ar.data.rdb.Get(ctx, likeKey(userID))
	rv, err = get.Int64()
	if err == redis.Nil {
		return 0, nil
	}
	return
}

func (ar *userRepo) IncUserLike(ctx context.Context, userID string) error {
	_, err := ar.data.rdb.Incr(ctx, likeKey(userID)).Result()
	return err
}
