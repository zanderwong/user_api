package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
	"time"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

func (User) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "user"},
	}
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("id").StorageKey("user_id").MaxLen(16).NotEmpty().Unique(),
		field.String("username").NotEmpty().Unique(),
		field.String("email").NotEmpty().Unique(),
		field.String("display_name"),
		field.Bool("is_blocked"),
		field.Time("ctime").
			Default(time.Now).Annotations(&entsql.Annotation{Default: "CURRENT_TIMESTAMP"}).SchemaType(map[string]string{
			dialect.MySQL: "timestamp",
		}).Immutable(),
		field.Time("utime").
			Default(time.Now).UpdateDefault(time.Now).Annotations(&entsql.Annotation{Default: "CURRENT_TIMESTAMP"}).SchemaType(map[string]string{
			dialect.MySQL: "timestamp",
		}),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return nil
}
