package service

import (
	"context"
	"crypto/sha256"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"gitlab.com/zanderwong/user_api/internal/biz"
	"time"

	pb "gitlab.com/zanderwong/user_api/api/user/v1"
)

type UserServiceService struct {
	pb.UnimplementedUserServiceServer
	log  *log.Helper
	user *biz.UserUsecase
}

func NewUserServiceService(user *biz.UserUsecase, logger log.Logger) *UserServiceService {
	return &UserServiceService{
		log:  log.NewHelper(logger),
		user: user,
	}
}

func (s *UserServiceService) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserReply, error) {
	s.log.Infof("input data %v", req)
	id := fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf("%s_%d", req.Username, time.Now().UnixNano()))))[:16]
	err := s.user.Create(ctx, &biz.User{
		ID:          id,
		Username:    req.Username,
		Email:       req.Email,
		DisplayName: req.DisplayName,
	})
	return &pb.CreateUserReply{}, err
}
func (s *UserServiceService) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.UpdateUserReply, error) {
	s.log.Infof("input data %v", req)
	err := s.user.Update(ctx, req.UserId, &biz.User{
		Username:    req.Username,
		Email:       req.Email,
		DisplayName: req.DisplayName,
		IsBlocked:   req.IsBlocked,
	})
	return &pb.UpdateUserReply{}, err
}
func (s *UserServiceService) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*pb.DeleteUserReply, error) {
	s.log.Infof("input data %v", req)
	err := s.user.Delete(ctx, req.UserId)
	return &pb.DeleteUserReply{}, err
}
func (s *UserServiceService) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.GetUserReply, error) {
	s.log.Infof("input data %v", req)

	p, err := s.user.Get(ctx, req.UserId)
	if err != nil {
		return nil, err
	}
	return &pb.GetUserReply{
		User: &pb.User{
			UserId:      p.ID,
			Username:    p.Username,
			Email:       p.Email,
			DisplayName: p.DisplayName,
			IsBlocked:   p.IsBlocked,
			Like:        p.Like,
		},
	}, nil
}
func (s *UserServiceService) ListUser(ctx context.Context, req *pb.ListUserRequest) (*pb.ListUserReply, error) {
	ps, err := s.user.List(ctx)
	reply := &pb.ListUserReply{}
	for _, p := range ps {
		reply.Results = append(reply.Results, &pb.User{
			UserId:      p.ID,
			Username:    p.Username,
			Email:       p.Email,
			DisplayName: p.DisplayName,
			IsBlocked:   p.IsBlocked,
			Like:        p.Like,
		})
	}
	return reply, err
}
